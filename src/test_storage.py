import json
import os
import re
import subprocess
import sys
import universal_functions

import test_storage_menu

__author__ = 'sean'


def log_to_dict(log_file_name):
    def consense_results_list(input_results_list, input_num_seconds):
        list_counter = 0
        output_results_list = [0] * input_num_seconds
        for result in input_results_list:
            if list_counter % input_num_seconds == 0 \
                    and list_counter != 0:
                list_counter = 0
            output_results_list[list_counter] += result
            list_counter += 1
        return output_results_list

    x_read_time = []
    y_read_metric = []
    x_write_time = []
    y_write_metric = []
    try:
        with open(log_file_name, 'r') as log_file:
            # Parse each line of the log
            for log_line in log_file:
                line_tokens = []
                log_line = log_line.strip()  # Strip trailing/leading space
                for num in log_line.split(','):  # Read the line into a list
                    num = int(re.sub(r'\D+', '',
                                     num))  # Strip non-numbers, cast to int
                    line_tokens.append(num)  # Add the int to the list
                if line_tokens[2] == 0:  # Indicates a read result
                    # Divide by 1000 to get seconds
                    x_read_time.append(float(line_tokens[0] / 1000))
                    y_read_metric.append(line_tokens[1])
                elif line_tokens[2] == 1:  # Indicates a write result
                    # Divide by 1000 to get seconds
                    x_write_time.append(float(line_tokens[0] / 1000))
                    y_write_metric.append(line_tokens[1])
    except OSError as ose:
        print('ERROR:\t{}.'.format(ose))
        sys.exit(-1)

    num_seconds = 0
    last_time = 0
    if len(x_read_time) > 0:
        time_list = x_read_time
    elif len(x_write_time) > 0:
        time_list = x_write_time
    else:
        print('ERROR\tx_read_time and x_write_time are empty!')
        print('Cannot continue!')
        sys.exit(-1)
    for single_time in time_list:
        # Setup for the first line
        if num_seconds == 0:
            last_time = single_time
            num_seconds = 1
        # Look for the end of the current thread's progress
        else:
            if last_time < single_time:
                num_seconds += 1
                last_time = single_time
            else:
                break
    print('This test ran for {} seconds'.format(num_seconds))

    y_read_metric = consense_results_list(y_read_metric, num_seconds)
    y_write_metric = consense_results_list(y_write_metric, num_seconds)
    x_read_time = list(range(1, (num_seconds + 0)))
    x_write_time = list(range(1, (num_seconds + 0)))

    dict_of_lists = {'x_read_time': x_read_time,
                     'y_read_metric': y_read_metric,
                     'x_write_time': x_write_time,
                     'y_write_metric': y_write_metric}
    return dict_of_lists


# fio tests
def rand_read_test(the_arguments):
    # fio 4KB random read
    # SNIA PTS IOPS Test Conditions:
    #   3.1.2 OIO/Thread: Thread
    #   3.1.3 Thread Count: 16 (numjobs)
    #       + 1 OIO (iodepth)
    #   3.1.4 Data Pattern: Random
    #   3.2.1.2 Execute RND IO, per (R/W Mix %, Block Size), for 1 minute

    log_name = '4k_rand_read'
    log_with_path = the_arguments['output_path'] + log_name

    for fn in os.listdir(the_arguments['output_path']):
        if fn == '{}_iops.log'.format(log_name) \
                or fn == '{}.json'.format(log_name):
            try:
                print('Removing previous test result:', fn)
                os.remove('{0}{1}'.format(the_arguments['output_path'], fn))
            except OSError as ose:
                print('ERROR:\t{}.'.format(ose))
                sys.exit(-1)
    try:
        subprocess.check_call(['fio',
                               '--ioengine=libaio',
                               '--softrandommap=1',
                               '--direct=1',
                               '--thread',
                               '--iodepth=1',
                               '--numjobs=16',
                               '--bs=4k',
                               '--rw=randread',
                               '--time_based',
                               '--runtime={}'.format(
                                   the_arguments['Run Time']),
                               '--filename={}'.format(
                                   the_arguments['Device Under Test (DUT)']),
                               '--name={}'.format(log_name),
                               '--random_generator=tausworthe64',
                               '--per_job_logs=0',
                               # TODO test JSON logs
                               # '--output-format=json',
                               '--write_iops_log={}'.format(log_name),
                               '--log_avg_msec=1000'])
        # fio doesn't want to save files to anything but cwd so this is needed
        subprocess.check_call(['mv', '{}_iops.log'.format(log_name), 'output'])
        this_dict = log_to_dict('{}_iops.log'.format(log_with_path))
        with open('{}.json'.format(log_with_path), 'w+') as results_file:
            json.dump(this_dict, results_file, sort_keys=True, indent=4)
        return universal_functions.get_results_dict(
            this_dict['y_read_metric'],
            'Time (seconds)',
            'IOP/s',
            'fio 4KB random read test',
            the_arguments['DUT Description'])
    except subprocess.CalledProcessError as cpe:
        print('ERROR:\tfio encountered an error!')
        print('\t\tExit status:', cpe.returncode)
        sys.exit(cpe.returncode)


def rand_readwrite_test(the_arguments):
    # fio 4KB random read/write mix (65/35)
    # SNIA PTS IOPS Test Conditions:
    #   3.1.2 OIO/Thread: Thread
    #   3.1.3 Thread Count: 16 (numjobs)
    #       + 1 OIO (iodepth)
    #   3.1.4 Data Pattern: Random
    #   3.2.1.2 Execute RND IO, per (R/W Mix %, Block Size), for 1 minute

    log_name = '4k_rand_readwrite'
    log_with_path = the_arguments['output_path'] + log_name

    for fn in os.listdir(the_arguments['output_path']):
        if fn == '{}_iops.log'.format(log_name) \
                or fn == '{}.json'.format(log_name):
            try:
                print('Removing previous test result:', fn)
                os.remove(
                    '{0}{1}'.format(the_arguments['output_path'], fn))
            except OSError as ose:
                print('ERROR:\t{}.'.format(ose))
                sys.exit(-1)
    try:
        subprocess.check_call(['fio',
                               '--ioengine=libaio',
                               '--softrandommap=1',
                               '--direct=1',
                               '--thread',
                               '--iodepth=1',
                               '--numjobs=16',
                               '--bs=4k',
                               '--rw=randrw',
                               '--rwmixread=65',
                               '--rwmixwrite=35',
                               '--time_based',
                               '--runtime={}'.format(
                                   the_arguments['Run Time']),
                               '--filename={}'.format(
                                   the_arguments[
                                       'Device Under Test (DUT)']),
                               '--name={}'.format(log_name),
                               '--random_generator=tausworthe64',
                               '--per_job_logs=0',
                               '--write_iops_log={}'.format(log_name),
                               '--log_avg_msec=1000'])
        # fio doesn't want to save files to anything but cwd so this is needed
        subprocess.check_call(
            ['mv', '{}_iops.log'.format(log_name), 'output'])
        this_dict = log_to_dict('{}_iops.log'.format(log_with_path))
        with open('{}.json'.format(log_with_path),
                  'w+') as results_file:
            json.dump(this_dict, results_file, sort_keys=True, indent=4)
        results_list = [read + write for read, write in zip(
            this_dict['y_read_metric'], this_dict['y_write_metric'])]
        return universal_functions.get_results_dict(
            results_list,
            'Time (seconds)',
            'IOP/s',
            'fio 4KB random read/write test',
            the_arguments['DUT Description'])
    except subprocess.CalledProcessError as cpe:
        print('ERROR:\tfio encountered an error!')
        print('\t\tExit status:', cpe.returncode)
        sys.exit(cpe.returncode)


def seq_read_test(the_arguments):
    # fio 128KB sequential read
    log_name = '128k_seq_read'
    log_with_path = the_arguments['output_path'] + log_name

    for fn in os.listdir(the_arguments['output_path']):
        if fn == '{}_bw.log'.format(log_name) \
                or fn == '{}.json'.format(log_name):
            try:
                print('Removing previous test result:', fn)
                os.remove('{0}{1}'.format(the_arguments['output_path'], fn))
            except OSError as ose:
                print('ERROR:\t{}.'.format(ose))
                sys.exit(-1)

    try:
        subprocess.check_call(['fio',
                               '--ioengine=libaio',
                               '--softrandommap=1',
                               '--direct=1',
                               '--thread',
                               '--iodepth=1',
                               '--numjobs=16 ',
                               '--bs=128k',
                               '--rw=read',
                               '--time_based',
                               '--runtime={}'.format(
                                   the_arguments['Run Time']),
                               '--group_reporting',
                               '--filename={}'.format(
                                   the_arguments['Device Under Test (DUT)']),
                               '--name={}'.format(log_name),
                               '--per_job_logs=0',
                               '--write_bw_log={}'.format(log_name),
                               '--log_avg_msec=1000'])
        # fio doesn't want to save files to anything but cwd so this is needed
        subprocess.check_call(['mv', '{}_bw.log'.format(log_name), 'output'])
        this_dict = log_to_dict('{}_bw.log'.format(log_with_path))
        with open('{}.json'.format(log_with_path), 'w+') as results_file:
            json.dump(this_dict, results_file, sort_keys=True, indent=4)
        return universal_functions.get_results_dict(
            this_dict['y_read_metric'],
            'Time (seconds)',
            'Bandwidth (KB)',
            'fio 128KB sequential read test',
            the_arguments['DUT Description'])
    except subprocess.CalledProcessError as cpe:
        print('ERROR:\tfio encountered an error!')
        print('\t\tExit status:', cpe.returncode)
        sys.exit(cpe.returncode)


def seq_write_test(the_arguments):
    # fio 128k sequential write
    log_name = '128k_seq_write'
    log_with_path = the_arguments['output_path'] + log_name

    for fn in os.listdir(the_arguments['output_path']):
        if fn == '{}_bw.log'.format(log_name) \
                or fn == '{}.json'.format(log_name):
            try:
                print('Removing previous test result:', fn)
                os.remove('{0}{1}'.format(the_arguments['output_path'], fn))
            except OSError as ose:
                print('ERROR:\t{}.'.format(ose))
                sys.exit(-1)

    try:
        subprocess.check_call(['fio',
                               '--ioengine=libaio',
                               '--softrandommap=1',
                               '--direct=1',
                               '--thread',
                               '--iodepth=1',
                               '--numjobs=16 ',
                               '--bs=128k',
                               '--rw=write',
                               '--time_based',
                               '--runtime={}'.format(
                                   the_arguments['Run Time']),
                               '--group_reporting',
                               '--filename={}'.format(
                                   the_arguments['Device Under Test (DUT)']),
                               '--name={}'.format(log_name),
                               '--per_job_logs=0',
                               '--write_bw_log={}'.format(log_name),
                               '--log_avg_msec=1000'])
        # fio doesn't want to save files to anything but cwd so this is needed
        subprocess.check_call(['mv', '{}_bw.log'.format(log_name), 'output'])
        this_dict = log_to_dict('{}_bw.log'.format(log_with_path))
        with open('{}.json'.format(log_with_path), 'w+') as results_file:
            json.dump(this_dict, results_file, sort_keys=True, indent=4)
        return universal_functions.get_results_dict(
            this_dict['y_write_metric'],
            'Time (seconds)',
            'Bandwidth (KB)',
            'fio 128KB sequential write test',
            the_arguments['DUT Description'])
    except subprocess.CalledProcessError as cpe:
        print('ERROR:\tfio encountered an error!')
        print('\t\tExit status:', cpe.returncode)
        sys.exit(cpe.returncode)


def main():
    # Get the test arguments
    storage_arguments = test_storage_menu.get_storage_arguments()

    if os.path.exists('./output'):
        output_path = './output/'
    else:
        output_path = ''
    storage_arguments['output_path'] = output_path

    # uut_dev_name = test_storage_menu.get_disk()

    print('Executing 4KB random read test')
    rand_read_dict = rand_read_test(storage_arguments)
    universal_functions.results_to_line_graph(
        rand_read_dict,
        '{}storage-rand-read'.format(storage_arguments['output_path']))

    print('Executing 4KB random read/write test')
    rand_readwrite_dict = rand_readwrite_test(storage_arguments)
    universal_functions.results_to_line_graph(
        rand_readwrite_dict,
        '{}storage-rand-readwrite'.format(storage_arguments['output_path']))

    print('Executing 128KB sequential read test')
    seq_read_dict = seq_read_test(storage_arguments)
    universal_functions.results_to_line_graph(
        seq_read_dict,
        '{}storage-seq-read'.format(storage_arguments['output_path']))

    print('Executing 128KB sequential write test')
    seq_write_dict = seq_write_test(storage_arguments)
    universal_functions.results_to_line_graph(
        seq_write_dict,
        '{}storage-seq-write'.format(storage_arguments['output_path']))

    try:
        with open('raw_results.json', 'r+') as results_file:
            try:
                print('Updating master JSON results file...')
                json_dict = json.load(results_file)
                json_dict['storage_rand_read'] = rand_read_dict
                json_dict['storage_rand_readwrite'] = rand_readwrite_dict
                json_dict['storage_seq_read'] = seq_read_dict
                json_dict['storage_seq_write'] = seq_write_dict
                results_file.seek(0)
                json.dump(json_dict, results_file, sort_keys=True, indent=4)
            except ValueError:
                print('ERROR:\tvalue error!')
                print('File may be empty or contain invalid JSON.  '
                      'Will overwrite with current results.')
                json_dict = {'storage_rand_read': rand_read_dict,
                             'storage_rand_readwrite': rand_readwrite_dict,
                             'storage_seq_read': seq_read_dict,
                             'storage_seq_write': seq_read_dict}
                results_file.seek(0)
                json.dump(json_dict, results_file, sort_keys=True, indent=4)
            results_file.truncate()
    except FileNotFoundError:
        print('ERROR:\tfile raw_results.json does not exist.  Creating...')
        with open('raw_results.json', 'a+') as new_results_file:
            json_dict = {'storage_rand_read': rand_read_dict,
                         'storage_rand_readwrite': rand_readwrite_dict,
                         'storage_seq_read': seq_read_dict,
                         'storage_seq_write': seq_read_dict}
            json.dump(json_dict, new_results_file, sort_keys=True, indent=4)

if __name__ == '__main__':
    main()
