"""
This module handles backend menu tasks such as drawing the screen and mapping
keyboard input to their appropriate actions
"""

# References:
#  https://docs.python.org/3.4/howto/curses.html
#  https://www.python.org/dev/peps/pep-0257/

import curses

import my_textpad

__author__ = 'sean'


def _draw_menu(stdscr):
    """
    Draw the menu and handle keyboard input.

    :param stdscr: the screen; handled by curses.wrapper
    :return: the string representing the currently selected row
    """

    # Clear the screen otherwise transitions result in garbled screen states
    stdscr.clear()
    # Set the window size
    screen = stdscr.subwin(23, 79, 0, 0)
    screen.refresh()

    current_selected_row = 1
    new_selected_row = 0
    highlighted_key = ''

    # Menu loop until item is chosen with the <enter> key
    while True:
        # Only redraw the screen if the row selection changed
        if new_selected_row != current_selected_row:
            # Setup selected row for first iteration
            if new_selected_row == 0:
                new_selected_row = 1
            # Set selected row for 2nd and later iterations
            else:
                current_selected_row = new_selected_row

            # Initial row and column values
            index_row = 0
            index_column = 0

            # Print the menu header
            screen.addstr(index_row, index_column,
                          _the_menu_header,
                          curses.A_UNDERLINE)
            index_row += 1

            # Add each item to the menu
            counter = 0
            for dict_key, dict_value in _the_given_dict.items():
                # Highlight the selected row
                if index_row == current_selected_row:
                    screen.addstr(index_row, index_column,
                                  dict_key + ' - ' + dict_value,
                                  curses.A_STANDOUT)
                    highlighted_key = dict_key

                else:
                    screen.addstr(index_row, index_column,
                                  dict_key + ' - ' + _the_given_dict[dict_key])
                index_row += 1
                counter += 1

        # Get a character fro the keyboard
        key = screen.getch()
        # 'q' quits
        if key == ord('q'):
            break
        # enter selects the highlighted item
        elif key == ord('\n'):
            return highlighted_key
        # up arrow key selects the next row up
        # Both down arrow keys on my laptop register as 65
        elif key in (65, 258, curses.KEY_DOWN):
            if current_selected_row > 1:
                new_selected_row -= 1
        # Both down arrow keys on my laptop register as 66
        elif key in (66, 259, curses.KEY_UP):
            if current_selected_row < 0 + len(_the_given_dict):
                new_selected_row += 1


def _draw_input(stdscr):
    """
    Draw the message to the user and the input box.

    :param stdscr: the screen; handled by curses.wrapper
    :return: the string representing the user-input text
    """

    stdscr.clear()
    ncols = 30
    nlines = 1
    uly = 2
    ulx = 2
    stdscr.addstr(uly - 2, ulx, _message)
    screen = curses.newwin(nlines, ncols, uly, ulx)
    my_textpad.rectangle(stdscr, uly - 1, ulx - 1, uly + nlines, ulx + ncols)
    stdscr.refresh()
    return my_textpad.Textbox(screen).edit()


def _draw_notification(stdscr):
    """
    Draw the message to the notification window.

    :param stdscr: the screen; handled by curses.wrapper
    :return: the string representing the user-input text
    """

    stdscr.clear()
    ncols = 30
    nlines = 1
    uly = 2
    ulx = 2
    stdscr.addstr(uly - 2, ulx, _message)
    stdscr.addstr(uly, ulx, 'Press <enter> to go back.')
    screen = curses.newwin(nlines, ncols, uly + 1, ulx)
    stdscr.refresh()
    return my_textpad.Textbox(screen).edit()


def menu_caller(given_dict, given_menu_header):
    """
    Setup the menu variables and execute the wrapper call.

    :param given_dict: the dictionary of menu items (OrderedDict)
    :param given_menu_header: the menu title (String)
    :return: the string representing the user's selection
    """

    global _the_given_dict, _the_menu_header
    _the_menu_header = given_menu_header
    _the_given_dict = given_dict
    return curses.wrapper(_draw_menu)


def input_caller(message):
    """
    Setup the input screen variables and execute the wrapper call.

    :param message: the question or message to the user (String)
    :return: a string representing the user's input
    """

    global _message
    _message = message
    return curses.wrapper(_draw_input)


def notification_caller(message):
    """
    Setup the notification variables and execute the wrapper call.

    :param message: the message to the user (String)
    :return: a string representing the user's input
    """

    global _message
    _message = message
    return curses.wrapper(_draw_notification)