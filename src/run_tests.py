import collections
import json
import os
import re
import subprocess
import time

import test_cpu
import test_memory
import test_net
import test_net_menu
import test_storage
import test_storage_menu
import universal_functions

__author__ = 'sean'

# Get the test arguments
iperf_arguments = test_net_menu.get_net_arguments()
# Get the storage test arguments
storage_arguments = test_storage_menu.get_storage_arguments()

# Set the output path
if os.path.exists('./output'):
    output_path = './output/'
else:
    output_path = ''

# Use serial number 284691103001 for testing
# export serial_number=284691103001

# Get the platform name
platform_name = universal_functions.get_os_info_t(
    'python2 /mnt/smbmnt/FTP/new-mbx-menu/APIdata.py platform name')[0]
customer_name = universal_functions.get_os_info_t(
    'python2 /mnt/smbmnt/FTP/new-mbx-menu/APIdata.py order customer_name')[0]

# START CPU TESTS
# Determine number of threads to be used for tests
# https://docs.python.org/3/library/os.html#os.cpu_count
try:
    cpu_threads = os.cpu_count()
    if not cpu_threads:
        print('ERROR:\tunable to determine the number of CPUs.  '
              'os.cpu_count() requires Python 3.4+!')
        print('Defaulting to two threads...')
        cpu_threads = 2
except NotImplementedError:
    print(
        'ERROR:\tos.cpu_count() not implemented.  Python 3.4+ is required!')
    print('Defaulting to two threads...')
    cpu_threads = 2

tachyon_dict = test_cpu.tachyon_test(cpu_threads, './input/teapot.dat')
universal_functions.results_to_bar_graph(
    tachyon_dict,
    '{}processor-tachyon'.format(output_path))

pbzip2_dict = test_cpu.pbzip2_test(cpu_threads)
universal_functions.results_to_bar_graph(
    pbzip2_dict,
    '{}processor-pbzip2'.format(output_path))
# END CPU TESTS

# START MEMORY TESTS
ramsmp_int_dict = test_memory.run_ramspeedtest('3', platform_name)
universal_functions.results_to_bar_graph(
    ramsmp_int_dict,
    '{}memory-ramsmp-int'.format(output_path))

ramsmp_float_dict = test_memory.run_ramspeedtest('6', platform_name)
universal_functions.results_to_bar_graph(
    ramsmp_float_dict,
    '{}memory-ramsmp-float'.format(output_path))
# END MEMORY TESTS

# START NETWORK TESTS

# Run bandwidth test
net_bw_dict = test_net.bw_test(iperf_arguments)
universal_functions.results_to_line_graph(
    net_bw_dict,
    '{}net-bandwidth'.format(output_path))

# The second iperf test fails to connect to the server if we don't pause
# for a few seconds
time.sleep(3)

# Run jitter test
net_jitter_dict = test_net.jitter_test(iperf_arguments)
universal_functions.results_to_line_graph(
    net_jitter_dict,
    '{}net-jitter'.format(output_path))
# END NETWORK TESTS

# START STORAGE TESTS
# TODO this seems inconsistent with other tests
# what can I do to make it consistent
storage_arguments['output_path'] = output_path
print('Executing 4KB random read test')
rand_read_dict = test_storage.rand_read_test(storage_arguments)
universal_functions.results_to_line_graph(
    rand_read_dict,
    '{}storage-rand-read'.format(output_path))

print('Executing 4KB random read/write test')
rand_readwrite_dict = test_storage.rand_readwrite_test(storage_arguments)
universal_functions.results_to_line_graph(
    rand_readwrite_dict,
    '{}storage-rand-readwrite'.format(output_path))

print('Executing 128KB sequential read test')
seq_read_dict = test_storage.seq_read_test(storage_arguments)
universal_functions.results_to_line_graph(
    seq_read_dict,
    '{}storage-seq-read'.format(output_path))

print('Executing 128KB sequential write test')
seq_write_dict = test_storage.seq_write_test(storage_arguments)
universal_functions.results_to_line_graph(
    seq_write_dict,
    '{}storage-seq-write'.format(output_path))
# END STORAGE TESTS

# START UPDATE OF JSON RESULTS DUMP
# Create or update raw_results.json
try:
    with open('raw_results.json', 'r+') as results_file:
        try:
            print('Updating master JSON results file...')
            json_dict = json.load(results_file)
            json_dict['processor_tachyon'] = tachyon_dict
            json_dict['processor_pbzip2'] = pbzip2_dict
            json_dict['memory_ramsmp_int'] = ramsmp_int_dict
            json_dict['memory_ramsmp_float'] = ramsmp_float_dict
            json_dict['net_bandwidth'] = net_bw_dict
            json_dict['net_jitter'] = net_jitter_dict
            json_dict['storage_rand_read'] = rand_read_dict
            json_dict['storage_rand_readwrite'] = rand_readwrite_dict
            json_dict['storage_seq_read'] = seq_read_dict
            json_dict['storage_seq_write'] = seq_write_dict
            results_file.seek(0)
            json.dump(json_dict, results_file, sort_keys=True, indent=4)
        except ValueError:
            print('ERROR:\tvalue error!')
            print('File may be empty or contain invalid JSON.'
                  'Will overwrite with current results.')
            json_dict = {'processor_tachyon': tachyon_dict,
                         'processor_pbzip2': pbzip2_dict,
                         'memory_ramsmp_int': ramsmp_int_dict,
                         'memory_ramsmp_float': ramsmp_float_dict,
                         'net_bw_dict': net_bw_dict,
                         'net_jitter_dict': net_jitter_dict,
                         'storage_rand_read': rand_read_dict,
                         'storage_rand_readwrite': rand_readwrite_dict,
                         'storage_seq_read': seq_read_dict,
                         'storage_seq_write': seq_read_dict}
            results_file.seek(0)
            json.dump(json_dict, results_file, sort_keys=True, indent=4)
        results_file.truncate()
except FileNotFoundError:
    print('ERROR:\tfile raw_results.json does not exist.  Creating...')
    with open('raw_results.json', 'a+') as new_results_file:
        json_dict = {'processor_tachyon': tachyon_dict,
                     'processor_pbzip2': pbzip2_dict,
                     'memory_ramsmp_int': ramsmp_int_dict,
                     'memory_ramsmp_float': ramsmp_float_dict,
                     'net_bw_dict': net_bw_dict,
                     'net_jitter_dict': net_jitter_dict,
                     'storage_rand_read': rand_read_dict,
                     'storage_rand_readwrite': rand_readwrite_dict,
                     'storage_seq_read': seq_read_dict,
                     'storage_seq_write': seq_read_dict}
        json.dump(json_dict, new_results_file, sort_keys=True, indent=4)
# END UPDATE OF JSON RESULTS DUMP

# BEGIN CREATION OF REPORT DOCUMENT
# Dynamically generate the list of parts for the hardware configuration table
# Generate the parts dictionary
srv_parts = []
cas_parts = []
mb_parts = []
cpu_parts = []
mem_parts = []
ssd_parts = []
hdd_parts = []
net_parts = []
rc_parts = []
vid_parts = []
fan_parts = []
lcd_parts = []
sh_parts = []  # TODO remove this line after testing

# An OrderedDict will maintain order in which the items are added
# This will be important when the data structure is iterated over to generate
# the list of parts for the report
# The unexpected arguments warning in pycharm is a bug :(
# https://youtrack.jetbrains.com/issue/PY-17759
items_dict = collections.OrderedDict([('SRV', srv_parts),
                                      ('CAS', cas_parts),
                                      ('MB', mb_parts),
                                      ('CPU', cpu_parts),
                                      ('MEM', mem_parts),
                                      ('SSD', ssd_parts),
                                      ('HDD', hdd_parts),
                                      ('NET', net_parts),
                                      ('RC', rc_parts),
                                      ('VID', vid_parts),
                                      ('FAN', fan_parts),
                                      ('LCD', lcd_parts),
                                      # TODO remove these after testing
                                      ('SH', sh_parts)])

order_line_items = universal_functions.get_os_info_t(
    'python2 /mnt/smbmnt/FTP/new-mbx-menu/APIdata.py line_items')
for line in order_line_items:
    part_number, part_quantity = str.split(line)
    part_category = universal_functions.get_os_info_t(
        'python2 /mnt/smbmnt/FTP/new-mbx-menu/APIdata.py item {} '
        'category'.format(part_number))[0]
    if part_category in items_dict:
        part_name = universal_functions.get_os_info_t(
            'python2 /mnt/smbmnt/FTP/new-mbx-menu/APIdata.py item {} '
            'name'.format(part_number))[0]
        this_part = {'part_name': part_name,
                     'part_number': part_number,
                     'part_quantity': part_quantity}
        items_dict[part_category].append(this_part)

# Generate a list of lines representing the hardware table
hardware_table = ['\t\t\section{Hardware Configuration}\n',
                  '\t\t\t\\begin{tabular}{lp{.7\linewidth}}\n',
                  '\t\t\t\t\\textbf{Quantity} & \\textbf{Part}\\\\[5pt]\n']
for part_category in items_dict:
    for part in items_dict[part_category]:
        # Replace underscores because they are 'special' in LaTeX
        if '_' in part['part_name']:
            part['part_name'] = re.sub('_',
                                       '\\TextUnderscore{}',
                                       part['part_name'])
        hardware_table.append('\t\t\t\t{} & {}\\\\\n'.format(
            int(float(part['part_quantity'])), part['part_name']))
hardware_table.append('\t\t\t\end{tabular}\n')

# Dynamically replace the generic company and model strings
# and insert the hardware table
with open('docs/ptype_report_template.tex', 'r') as tex_file:
    tex_lines = []
    for line in tex_file:
        if re.search('Generic-Company', line):
            print('old: {}'.format(line))
            line = re.sub('Generic-Company', customer_name, line)
            print('new: {}'.format(line))
        if re.search('Generic-Model', line):
            print('old: {}'.format(line))
            line = re.sub('Generic-Model', platform_name, line)
            print('new: {}'.format(line))
        # If the line is where the hardware table goes add the table
        if re.search('%hardware_table', line):
            for row in hardware_table:
                tex_lines.append(row)
        # Otherwise just append the line
        else:
            tex_lines.append(line)
    with open('docs/ptype_report.tex', 'w+') as new_tex_file:
        for line in tex_lines:
            new_tex_file.write(line)

# Generate a PDF report from the latex markup
os.chdir('./docs')
subprocess.check_call(['pdflatex',
                       '-synctex=1',
                       '-interaction=nonstopmode',
                       '"ptype_report".tex'])
os.remove('ptype_report.aux')
os.remove('ptype_report.log')
os.remove('ptype_report.out')
os.remove('ptype_report.synctex.gz')
os.remove('ptype_report.toc')

# END CREATION OF REPORT DOCUMENT
