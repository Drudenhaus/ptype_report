#  References:
#   https://docs.python.org/3/library/curses.html
#   http://amitsaha.github.io/site/notes/articles/python_linux/article.html

import collections
import glob
import re
import os
import sys

import menu_backend

from menu_backend import menu_caller
from universal_functions import get_os_info_t

__author__ = 'sean'


def get_storage_arguments():
    """
    Define test defaults
    Translate menu selections to matching input_caller function calls
    Return the test configuration (OrderedDict) when user is done.
    """
    # This is the String that will be displayed to the user
    menu_header = 'Select an item to configure:'
    # Setup the dictionary with its defaults
    # The unexpected arguments warning in pycharm is a bug :(
    # https://youtrack.jetbrains.com/issue/PY-17759
    storage_dict = collections.OrderedDict([
        ('Device Under Test (DUT)', '/dev/sda'),
        ('DUT Description', 'DUT Model'),
        ('Run Time', '60'),
        ('Done', 'Run Test'),
        ('Quit', 'Exit to shell')
    ])

    # Handle the different menu options as the user selects them
    while True:
        selection = menu_backend.menu_caller(storage_dict, menu_header)
        if selection == 'Device Under Test (DUT)':
            new_value = get_disk()
            # Only update the value if it's a valid string
            if len(new_value) > 0:
                storage_dict['Device Under Test (DUT)'] = new_value
        if selection == 'DUT Description':
            new_value = menu_backend.input_caller(
                'DUT Description:').strip()
            # Only update the value if it's a valid string
            if len(new_value) > 0:
                storage_dict['DUT Description'] = new_value
        elif selection == 'Run Time':
            new_value = menu_backend.input_caller('Run time (seconds):').strip()
            # Only update the value if it's a valid (positive) integer
            if len(new_value) > 0:
                if new_value.isdigit():
                    storage_dict['Run Time'] = new_value
                else:
                    menu_backend.notification_caller(
                        'ERROR: invalid quantity of time.')
        elif selection == 'Done':
            return storage_dict
        # TODO implement storage_parms class then test this
        # elif selection == 'Save':
        #     # Save the storage arguments to a picked object
        #     try:
        #         # Code for using the configparser methodology
        #         # Note: if switching, change open mode to non-binary
        #         new_test = storage_parms.TestInstance()
        #         new_test.dut = storage_dict['Device Under Test (DUT)']
        #         new_test.dut_description = storage_dict['DUT Description']
        #         new_test.run_time = storage_dict['Run Time']
        #         new_test.save('test_storage.cfg')
        #     except OSError:
        #         menu_backend.notification_caller(
        #             'ERROR: failed to open test_storage.cfg.')
        # elif selection == 'Load':
        #     # Load the storage arguments from the configparser file
        #     # Then copy the pertinent values from the imported object to
        #     # the storage_dict object
        #     try:
        #         new_test = storage_parms.TestInstance()
        #         if new_test.load('test_storage.cfg'):
        #             storage_dict['Device Under Test (DUT)'] = new_test.dut
        #             storage_dict['DUT Description'] = new_test.dut_description
        #             storage_dict['Run Time'] = new_test.run_time
        #     except OSError:
        #         menu_backend.notification_caller(
        #             'ERROR: failed to open test_storage.cfg.')
        elif selection == 'Quit':
            sys.exit(0)


def get_disk():
    # This function builds the prerequisite matrix
    # and calls the generic menu function

    # Disk device patterns.
    # By default, I'm only including sd* devices (e.g. sda)
    disk_dev_patterns = ['sd.*']

    disk_dev_list = []
    disk_description_list = []
    menu_header = 'Select the disk to test:'

    # Loop over each item in /sys/block that matches a pattern in the list
    for sys_path_dev in glob.glob('/sys/block/*'):
        for disk_dev_pattern in disk_dev_patterns:
            # If the device matches add the /dev path to disk_dev_list
            # and the model in disk_description_list
            if re.compile(disk_dev_pattern).match(os.path.basename(
                    sys_path_dev)):
                disk_dev_list.append('/dev/{}'.format(
                    os.path.basename(sys_path_dev)))
                with open('{}/device/model'.format(sys_path_dev), 'r') as file:
                    disk_description = file.readline().strip()
                disk_description_list.append(disk_description)

    # Create the OrderedDict from the two lists
    # The unexpected arguments warning in pycharm is a bug :(
    # https://youtrack.jetbrains.com/issue/PY-17759
    storage_dict = collections.OrderedDict(zip(
        disk_dev_list, disk_description_list))

    # Call the generic menu which will allow the user to select a disk
    # A string with the disk's /dev path will be returned
    return menu_caller(storage_dict, menu_header)


# TODO delete this function if it isn't needed
def get_partition(disk_dev):
    # This function builds the prerequisite matrix
    # and calls the generic menu function

    partition_dev_list = []
    partition_fs_list = []
    menu_header = 'Select the partition to test:'

    # Loop over each item in the blkid output
    # TODO use python regex instead of awk (used awk for familiarity reasons)
    # Some binary distros include outdated versions of lsblk (packages) that do
    # not support -p for full paths.
    # Resultantly, I omitted -p and concatenated a literal path with the device
    for partition_dev in get_os_info_t(
            'lsblk -o NAME,TYPE -nl {} | awk \'/part/{{print $1}}\''.format(
                disk_dev)):
        partition_dev_list.append('/dev/{}'.format(partition_dev))
        partition_fs_list.append(get_os_info_t(
            'blkid -s TYPE -o value {}'.format(
                '/dev/{}'.format(partition_dev)))[0])

    # Create the OrderedDict from the two lists
    # The unexpected arguments warning in pycharm is a bug :(
    # https://youtrack.jetbrains.com/issue/PY-17759
    storage_dict = collections.OrderedDict(zip(
        partition_dev_list, partition_fs_list))

    # Call the generic menu which will allow the user to select a partition
    # A string with the partition's /dev path will be returned
    return menu_caller(storage_dict, menu_header)
