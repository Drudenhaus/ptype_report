"""
This is a helper module that works in conjunction with the menu_backend module.
It sets up the defaults and handles the user's specific selections.
The test configuration (OrderedDict) is returned when the user is done.
"""

import collections
import socket
import sys

import iperf_args
import menu_backend

__author__ = 'sean'


def get_net_arguments():
    """
    Define test defaults
    Translate menu selections to matching input_caller function calls
    Return the test configuration (OrderedDict) when user is done.
    """
    # This is the String that will be displayed to the user
    menu_header = 'Select an item to configure:'
    # Setup the dictionary with its defaults
    iperf_dict = collections.OrderedDict([
        ('Client IP Address', '192.168.0.2'),
        ('Server IP Address', '192.168.0.100'),
        ('Run Time', '900'),
        ('Test Card Model', 'Undefined'),
        ('Done', 'Run Test'),
        ('Save', 'Save to test_net.cfg'),
        ('Load', 'Load from test_net.cfg'),
        ('Quit', 'Exit to shell')
    ])

    # Handle the different menu options as the user selects them
    while True:
        selection = menu_backend.menu_caller(iperf_dict, menu_header)
        if selection == 'Client IP Address':
            new_value = menu_backend.input_caller('Client IP address:').strip()
            # Validate the input is a valid IP address
            # This accepts less than 4 octets as it uses Unix' inet for
            # implementation.  See inet man page for more details.
            # Invalid IPs cause an OSError such as:
            # OSError: illegal IP address string passed to inet_aton
            if len(new_value) > 0:
                try:
                    socket.inet_aton(new_value)
                    iperf_dict['Client IP Address'] = new_value
                except OSError:
                    menu_backend.notification_caller(
                        'ERROR: invalid IP address.')
                except socket.error:
                    menu_backend.notification_caller(
                        'ERROR: invalid IP address.')
        elif selection == 'Server IP Address':
            new_value = menu_backend.input_caller('Server IP address:').strip()
            # Validate the input is a valid IP address
            # Invalid IPs cause an OSError such as:
            # OSError: illegal IP address string passed to inet_aton
            if len(new_value) > 0:
                try:
                    socket.inet_aton(new_value)
                    iperf_dict['Server IP Address'] = new_value
                except OSError:
                    menu_backend.notification_caller(
                        'ERROR: invalid IP address.')
                except socket.error:
                    menu_backend.notification_caller(
                        'ERROR: invalid IP address.')
        elif selection == 'Run Time':
            new_value = menu_backend.input_caller('Run time (seconds):').strip()
            # Only update the value if it's a valid (positive) integer
            if len(new_value) > 0:
                if new_value.isdigit():
                    iperf_dict['Run Time'] = new_value
                else:
                    menu_backend.notification_caller(
                        'ERROR: invalid quantity of time.')
        elif selection == 'Test Card Model':
            new_value = menu_backend.input_caller('Card model:').strip()
            # Only update the value if it's a valid string
            if len(new_value) > 0:
                iperf_dict['Test Card Model'] = new_value
        elif selection == 'Done':
            return iperf_dict
        elif selection == 'Save':
            # Save the iperf arguments to a picked object
            try:
                # Code for using the configparser methodology
                # Note: if switching, change open mode to non-binary
                new_test = iperf_args.TestInstance()
                new_test.client_ip = iperf_dict['Client IP Address']
                new_test.server_ip = iperf_dict['Server IP Address']
                new_test.run_time = iperf_dict['Run Time']
                new_test.test_card_model = iperf_dict['Test Card Model']
                new_test.save('test_net.cfg')
            except OSError:
                menu_backend.notification_caller(
                    'ERROR: failed to open test_net.cfg.')
        elif selection == 'Load':
            # Load the iperf arguments from the configparser file
            # Then copy the pertinent values from the imported object to
            # the iperf_dict object
            try:
                new_test = iperf_args.TestInstance()
                if new_test.load('test_net.cfg'):
                    iperf_dict['Client IP Address'] = new_test.client_ip
                    iperf_dict['Server IP Address'] = new_test.server_ip
                    iperf_dict['Run Time'] = new_test.run_time
                    iperf_dict['Test Card Model'] = new_test.test_card_model
            except OSError:
                menu_backend.notification_caller(
                    'ERROR: failed to open test_net.cfg.')
        elif selection == 'Quit':
            sys.exit(0)
