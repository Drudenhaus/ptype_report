"""
This is the core network testing module.  It obtains the iperf arguments
through the use of the menu test_net_menu, runs the iperf test itself, parses
the JSON output, graphs the data, and outputs .png images representing the test
results.
"""

import collections
import json
import math
import os
import sys
import subprocess
import time

import test_net_menu
import universal_functions

__author__ = 'sean'


# iperf tests
def bw_test(the_arguments):
    try:
        # Remove old JSON files.  iperf will append to them rather than replace
        # them.  This causes parsing issues because the jitter JSON file will
        # contain a nested JSON object.
        for fn in os.listdir('.'):
            if fn == 'net_bw.json':
                try:
                    print('Removing previous net testing results:', fn)
                    os.remove(fn)
                except OSError as ose:
                    print('ERROR:\t{}.'.format(ose))
                    sys.exit(-1)

        # Bandwidth test
        print('Running bandwidth test...')
        subprocess.check_call(['iperf3', '--client',
                               the_arguments['Server IP Address'],
                               '--window', '10m',
                               '--time', the_arguments['Run Time'],
                               '--omit', '10',
                               '--interval', '1',
                               '--length', '64k ',
                               '--parallel', '4',
                               '--json',
                               '--logfile', 'net_bw.json',
                               '--bind', the_arguments['Client IP Address']])
        print('Done!')
        # Get and graph the bandwidth results
        with open('net_bw.json', 'r') as net_bw_json_file:
            net_bw_json_blob = json.load(net_bw_json_file,
                                         object_pairs_hook=collections.
                                         OrderedDict)
            net_bw_list = get_mbits_per_second(net_bw_json_blob,
                                               net_bw_json_blob['intervals'])
            return universal_functions.get_results_dict(
                net_bw_list,
                'Time (seconds)',
                'Megabits',
                'iperf3 Bandwidth Test',
                the_arguments['Test Card Model'])
    except subprocess.CalledProcessError as cpe:
        print('ERROR:\tiperf encountered an error!')
        print('\tExit status:', cpe.returncode)
        sys.exit(cpe.returncode)
    except OSError as ose:
        print('ERROR:\tiperf3 not found!')
        print('\tIs iperf3 installed and in your $PATH variable?')
        sys.exit(ose.errno)


def jitter_test(the_arguments):
    try:
        # Remove old JSON files.  iperf will append to them rather than replace
        # them.  This causes parsing issues because the jitter JSON file will
        # contain a nested JSON object.
        for fn in os.listdir('.'):
            if fn == 'net_jitter.json':
                try:
                    print('Removing previous net testing results:', fn)
                    os.remove(fn)
                except OSError as ose:
                    print('ERROR:\t{}.'.format(ose))
                    sys.exit(-1)
        print('Running jitter test...')
        subprocess.check_call(['iperf3', '--client',
                               the_arguments['Server IP Address'],
                               '--window', '10m',
                               '--time', the_arguments['Run Time'],
                               '--omit', '10',
                               '--interval', '1',
                               '--udp',
                               '--json',
                               '--logfile', 'net_jitter.json',
                               '--get-server-output',
                               '--bind', the_arguments['Client IP Address']])
        print('Done!')
        with open('net_jitter.json', 'r') as net_jitter_json_file:
            net_jitter_json_blob = json.load(net_jitter_json_file,
                                             object_pairs_hook=collections.
                                             OrderedDict)
            # Position = 10 is necessary to skip the first 10 seconds.
            # The server still logs entries belonging to the omitted entries
            # (omit = 10)
            net_jitter_list = get_jitter_us(net_jitter_json_blob,
                                            net_jitter_json_blob
                                            ['server_output_json']['intervals'])
            return universal_functions.get_results_dict(
                net_jitter_list,
                'Time (seconds)',
                'Jitter (\u00B5s)',
                'iperf3 Jitter Test',
                the_arguments['Test Card Model'])
    except subprocess.CalledProcessError as cpe:
        print('ERROR:\tiperf encountered an error!')
        print('\tExit status:', cpe.returncode)
        sys.exit(cpe.returncode)
    except OSError as ose:
        print('ERROR:\tiperf3 not found!')
        print('\tIs iperf3 installed and in your $PATH variable?')
        sys.exit(ose.errno)


# Convert bits per second to megabits per second
# Round down and return an int
def bits_to_mbits_per_second(bits):
    """
    Take a number of bits and return the converted quantity of megabits

    :param bits: an integer representing a quantity of bits
    :return: an integer representing bits converted to megabits
    """
    return math.floor(bits / math.pow(1000, 2))


def ms_to_us(ms):
    """
    Take a number of milliseconds and return the converted quantity of
    microseconds.

    :param ms: an integer representing a quantity of milliseconds.
    :return: an integer representing bits converted to microseconds.
    """
    return ms * math.pow(1000, 1)


# Get bits per second results and convert each result to mbits per second
def get_mbits_per_second(json_blob, the_dict):
    """
    Pull the megabits per second results out of the JSON results and,
    ultimately, return a list of those results

    :param json_blob: the raw JSON output from iperf.
    :param the_dict: resulting dict from the interval key of the JSON results.
    :return: the megabits per second results in a list
    """

    the_list = []
    # Skip the first 10 seconds (the test will stabilize by this time)
    for position in range(10, len(the_dict)):
        bits = json_blob['intervals'][position]['sum']['bits_per_second']
        mbits = bits_to_mbits_per_second(bits)
        the_list.append(mbits)
    return the_list


# Get get latency results in ms results and convert each result to us
def get_jitter_us(json_blob, the_dict):
    """
    Pull the jitter results out of the JSON results and, ultimately, return a
    list of those results

    :param json_blob: the raw JSON output from iperf.
    :param the_dict: resulting dict from the interval key of the JSON results.
    :return: the jitter results in a list
    """

    the_list = []
    # Skip the first 10 seconds (the test will stabilize by this time)
    for position in range(10, len(the_dict)):
        ms = json_blob['server_output_json']['intervals'][position]['streams'][
            0]['jitter_ms']
        us = ms_to_us(ms)
        the_list.append(us)
    return the_list


def main():
    # Get the test arguments
    iperf_arguments = test_net_menu.get_net_arguments()

    if os.path.exists('./output'):
        output_path = './output/'
    else:
        output_path = ''

    # Run bandwidth test
    net_bw_dict = bw_test(iperf_arguments)
    universal_functions.results_to_line_graph(
        net_bw_dict,
        '{}net-bandwidth'.format(output_path))

    # The second iperf test fails to connect to the server if we don't pause
    # for a few seconds
    time.sleep(3)

    # Run jitter test
    net_jitter_dict = jitter_test(iperf_arguments)
    universal_functions.results_to_line_graph(
        net_jitter_dict,
        '{}net-jitter'.format(output_path))

    try:
        with open('raw_results.json', 'r+') as results_file:
            try:
                print('Updating master JSON results file...')
                json_dict = json.load(results_file)
                json_dict['net_bandwidth'] = net_bw_dict
                json_dict['net_jitter'] = net_jitter_dict
                results_file.seek(0)
                json.dump(json_dict, results_file, sort_keys=True, indent=4)
            except ValueError:
                print('ERROR:\tvalue error!  File may be empty or contain '
                      'invalid JSON.  Will overwrite with current results.')
                json_dict = {'net_bw_dict': net_bw_dict,
                             'net_jitter_dict': net_jitter_dict}
                results_file.seek(0)
                json.dump(json_dict, results_file, sort_keys=True, indent=4)
            results_file.truncate()
    except FileNotFoundError:
        print('ERROR:\tfile raw_results.json does not exist.  Creating...')
        with open('raw_results.json', 'a+') as new_results_file:
            json_dict = {'net_bw_dict': net_bw_dict,
                         'net_jitter_dict': net_jitter_dict}
            json.dump(json_dict, new_results_file, sort_keys=True, indent=4)

if __name__ == '__main__':
    main()
