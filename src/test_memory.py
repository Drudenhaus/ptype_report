"""
This is the core memory testing module.  It runs a predefined set of tests,
saves the results to a master JSON results file, and outputs .png images
representing the test results.
"""

import json
import os
import re
import subprocess
import sys

import universal_functions

__author__ = 'sean'

# TODO create reference data


def run_ramspeedtest(test_type, platform_name):
    """
    Run the ramspeed test specified by the user.
    Only tests 3 and 6 (int and float) are currently supported.

    :param test_type: an integer representing the ramsmp test to be run
    :param platform_name: a string representing the platform name which will be
    used as a label in the resulting graphics
    :return: results_dict: dictionary containing test run information
    """
    try:
        # We could run any, but I'm only planning on using tests 3 and 6
        if test_type == '3':
            search_str = 'INTEGER'
            test_name = 'RAMspeed/SMP Integer Test'
        elif test_type == '6':
            search_str = 'FL-POINT'
            test_name = 'RAMspeed/SMP Floating Point Test'
        else:
            print('Unsupported test type!')
            print('Only test types 3 and 6 are currently supported.')
            sys.exit(-1)
        print('Running ramspeed {} test...'.format(search_str))
        output_list = subprocess.check_output(['ramsmp',
                                               '-b', test_type,
                                               'm', '64',
                                               '-l', '10']
                                              ).decode('ascii').splitlines()
        result_list = []
        for item in output_list:
            if re.search('{}\\s+AVERAGE'.format(search_str), item):
                result_list.append(float(item.split()[-2]))
        results_dict = universal_functions.get_results_dict(
            result_list,
            'Test Run',
            'Bandwidth (MB/s)',
            test_name,
            platform_name)
        print('Mean:\t\t\t',
              str('{:.2f}'.format(results_dict['mean'])), 'MB/s')
        print('Median:\t\t\t',
              str('{:.2f}'.format(results_dict['median'])), 'MB/s')
        print('Standard Deviation:\t',
              str('{:.2f}'.format(results_dict['stddev'])), 'MB/s')
        return results_dict
    except subprocess.CalledProcessError as cpe:
        print('ERROR:\tramsmp encountered an error!')
        print('\t\tExit status:', cpe.returncode)
        sys.exit(cpe.returncode)


def main():
    if os.path.exists('./output'):
        output_path = './output/'
    else:
        output_path = ''

    # TODO provide the platform name to this function
    ramsmp_int_dict = run_ramspeedtest('3', 'platform name')
    universal_functions.results_to_bar_graph(
        ramsmp_int_dict,
        '{}memory-ramsmp-int'.format(output_path))

    # TODO provide the platform name to this function
    ramsmp_float_dict = run_ramspeedtest('6', 'platform name')
    universal_functions.results_to_bar_graph(
        ramsmp_float_dict,
        '{}memory-ramsmp-float'.format(output_path))

    try:
        with open('raw_results.json', 'r+') as results_file:
            try:
                print('Updating master JSON results file...')
                json_dict = json.load(results_file)
                json_dict['memory_ramsmp_int'] = ramsmp_int_dict
                json_dict['memory_ramsmp_float'] = ramsmp_float_dict
                results_file.seek(0)
                json.dump(json_dict, results_file, sort_keys=True, indent=4)
            except ValueError:
                print('ERROR:\tvalue error!  File may be empty or contain '
                      'invalid JSON.  Will overwrite with current results.')
                json_dict = {'memory_ramsmp_int': ramsmp_int_dict,
                             'memory_ramsmp_float': ramsmp_float_dict}
                results_file.seek(0)
                json.dump(json_dict, results_file, sort_keys=True, indent=4)
            results_file.truncate()
    except FileNotFoundError:
        print('ERROR:\tfile raw_results.json does not exist.  Creating...')
        with open('raw_results.json', 'a+') as new_results_file:
            json_dict = {'memory_ramsmp_int': ramsmp_int_dict,
                         'memory_ramsmp_float': ramsmp_float_dict}
            json.dump(json_dict, new_results_file, sort_keys=True, indent=4)

if __name__ == '__main__':
    main()
