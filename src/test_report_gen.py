import subprocess
import os

os.chdir('./docs')
subprocess.check_call(['pdflatex',
                       '-synctex=1',
                       '-interaction=nonstopmode',
                       '"ptype_report".tex'])
