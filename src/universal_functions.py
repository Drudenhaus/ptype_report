import math
import os
import re
import subprocess
import sys

import numpy
import matplotlib as mpl

# Use the agg backend so we don't need to rely on an X server/session
# This needs to be done before the pyplot import else the backend won't change
# and a warning is thrown
mpl.use('Agg')
import matplotlib.pyplot as plt

__author__ = 'sean'


# Required: linux OS
def verify_os_is_linux():
    if 'linux' not in sys.platform:
        print('I\'m only supporting Linux right now...')
        # I haven't tested any other platforms...
        sys.exit(253)


#  Required access: root
def verify_this_is_root_user():
    if os.getuid() != 0:
        print('ERROR: must be run as root!')
        sys.exit(255)


# Does not include a timeout period
def get_os_info(run_this):
    the_list = []
    the_info = ''
    try:
        proc = subprocess.Popen(run_this,
                                stdout=subprocess.PIPE,
                                shell=True,
                                # executable='/bin/bash',
                                universal_newlines=True)
        proc.wait()
        if proc.returncode == 0:
            for line in proc.stdout:
                the_info += str(line.strip())
                the_list.append(the_info)
                the_info = ''  # reset
            return the_list
        else:
            print('ERROR: process \'{}\' exited abnormally with code {}'.format(
                run_this, proc.returncode))
            sys.exit(-1)
    except ValueError:
        print('ERROR: invalid arguments in command \'{}\''.format(run_this))
        print('This can be caused by the distribution containing outdated '
              'binaries.')
        sys.exit(-1)


# This version includes a 180 second timeout
def get_os_info_t(run_this):
    timeout_value = 180
    the_list = []
    the_info = ''
    try:
        proc = subprocess.Popen(run_this,
                                stdout=subprocess.PIPE,
                                shell=True,
                                # executable='/bin/bash',
                                universal_newlines=True)
        proc.wait(timeout=timeout_value)
        if proc.returncode == 0:
            for line in proc.stdout:
                the_info += str(line.strip())
                the_list.append(the_info)
                the_info = ''  # reset
            return the_list
        else:
            print('ERROR: process \'{}\' exited abnormally with code {}'.format(
                run_this, proc.returncode))
            sys.exit(-1)
    except subprocess.TimeoutExpired:
        print('ERROR: process \'{}\' timed out after waiting {} seconds!'.
              format(run_this, timeout_value))
        sys.exit(-1)
    except ValueError:
        print('ERROR: invalid arguments in command \'{}\''.format(run_this))
        print('This can be caused by the distribution containing outdated '
              'binaries.')
        sys.exit(-1)


def get_num_physical_cpus():
    with open('/proc/cpuinfo') as proccpu:
        num_logical_cpus = 0
        for line in proccpu:
            if 'model name' in line.strip():
                num_logical_cpus += 1

    with open('/proc/cpuinfo') as proccpu:
        num_cpu_siblings = 0
        for line in proccpu:
            if 'siblings' in line.strip():
                num_cpu_siblings = int(re.sub(r'[^\d.]+', '', line))
                break
    return int(num_logical_cpus / num_cpu_siblings)


def get_cpu_model():
    # return platform.processor()
    with open('/proc/cpuinfo', mode='r') as proccpuinfo:
        for line in proccpuinfo:
            if line.strip().startswith('model name'):
                the_model = line.split(':')[1].strip()
                return the_model


def get_results_dict(results, xlabel, metric, test_name, model):
    results_dict = dict()
    results_dict['results_list'] = results
    results_dict['mean'] = numpy.mean(results)
    results_dict['median'] = numpy.median(results)
    results_dict['stddev'] = numpy.std(results)
    results_dict['xlabel'] = xlabel
    results_dict['metric'] = metric
    results_dict['test_name'] = test_name
    results_dict['model'] = model
    return results_dict


# Label bars in bar graphs
# Originally from: http://matplotlib.org/examples/api/barchart_demo.html
def autolabel_bar(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width() / 2.0, 1.01 * height,
                 # Use FP with two decimal places
                 '%0.2f' % float(height),
                 # '%d' % int(height),
                 ha='left', va='bottom', fontsize=10, rotation=45)


# TODO accommodate reference data set
def results_to_bar_graph(results_dict, filename):
    """
    Take the results dictionary and produce a bar graph representation

    :param results_dict: dictionary containing information regarding the test
    :param filename: the file name for the .png image
    :return:
    """
    results_list = results_dict['results_list']
    mean = results_dict['mean']
    stddev = results_dict['stddev']
    xlabel = results_dict['xlabel']
    ylabel = results_dict['metric']
    title = results_dict['test_name']
    model = results_dict['model']

    plt.clf()
    ax = plt.subplot(111)
    # Set the y-value range.  This ensures the bar labels will fit.
    # TODO test that the bar label of the last bar of the second data set fits
    ax.set_ylim(0, max(results_list) * 1.15)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    x_pos = numpy.arange(len(results_list))
    bar_width = 0.4

    bar = ax.bar(x_pos, results_list, bar_width, color='b', label=model,
                 yerr=stddev)
    autolabel_bar(bar)
    plt.xticks(x_pos + (bar_width / 2), range(1, len(results_list) + 1))
    # TODO use this xtick position for comparing two data sets
    # plt.xticks(x_pos + bar_width, range(1, len(results) + 1))

    # http://matplotlib.org/users/recipes.html#placing-text-boxes
    textstr = '$\mu=%.2f$\n$\sigma=%.2f$' % (mean, stddev)

    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle='round', facecolor='wheat')  # , alpha=0.5)

    # place a text box in upper left in axes coords
    plt.text(.97, 0.03, textstr, transform=ax.transAxes, fontsize=10,
             ha='right', va='bottom', bbox=props)

    ymin, ymax = plt.ylim()
    plt.ylim(ymin, ymax * 1.1)

    # Shrink the figure size so that the legend fits  below it
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width,
                     box.height * 0.85])
    # Add the legend
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), fancybox=True,
              ncol=len(results_list), fontsize=10)

    # Set the title
    plt.title(title)
    plt.savefig(filename + '.png', format='png')
    print('Saved:\t{}.png'.format(filename))


# TODO accommodate reference data set
def results_to_line_graph(results_dict, filename):
    """
    Take the results list and produce a line graph representation

    :param results_dict: dictionary containing information regarding the test
    :param filename: the file name for the .png image
    :return:
    """
    results_list = results_dict['results_list']
    mean = results_dict['mean']
    stddev = results_dict['stddev']
    xlabel = results_dict['xlabel']
    ylabel = results_dict['metric']
    title = results_dict['test_name']
    model = results_dict['model']
    plt.clf()
    ax = plt.subplot(111)

    # Set the y-value range.
    ax.set_ylim(min(results_list) * 0.25, max(results_list) * 1.15)
    ax.set_xlim(1, len(results_list))
    plt.xticks(numpy.arange(1,
                            len(results_list) + 1,
                            math.ceil(len(results_list) / 10)))
    # Grid documentation: http://matplotlib.org/api/pyplot_api.html
    ax.grid(color='gray', linestyle=':', linewidth=0.5)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.plot(range(1, len(results_list) + 1), results_list, label=model)
    # TODO uncomment to plot reference results
    # plt.plot(ref_results, label='Reference System')

    # http://matplotlib.org/users/recipes.html#placing-text-boxes
    textstr = '$\mu=%.2f$\n$\sigma=%.2f$' % (mean, stddev)

    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle='round', facecolor='wheat')  # , alpha=0.5)

    # place a text box in lower left in axes coords
    plt.text(.97, 0.03, textstr, transform=ax.transAxes, fontsize=10,
             ha='right', va='bottom', bbox=props)

    ymin, ymax = plt.ylim()
    plt.ylim(ymin, ymax * 1.1)

    # Shrink the figure size so that the legend fits below it
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width,
                     box.height * 0.85])

    # Add the legend
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), fancybox=True,
              ncol=len(results_list), fontsize=10)

    # Set the title
    plt.title(title)
    plt.savefig(filename + '.png', format='png')
    print('Saved:\t{}.png'.format(filename))
