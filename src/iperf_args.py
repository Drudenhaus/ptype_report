"""Object class for storing iperf test arguments"""

import configparser

import menu_backend

__author__ = 'sean'


class TestInstance:
    """The object itself class for storing iperf test arguments"""

    # Initially set all values to blank
    def __init__(self):
        self.client_ip = ''
        self.server_ip = ''
        self.run_time = ''
        self.test_card_model = ''

    # Property definitions
    def _set_client_ip(self, value):
        self._client_ip = value

    def _get_client_ip(self):
        return self._client_ip

    client_ip = property(_get_client_ip, _set_client_ip)

    def _set_server_ip(self, value):
        self._server_ip = value

    def _get_server_ip(self):
        return self._server_ip

    server_ip = property(_get_server_ip, _set_server_ip)

    def _set_run_time(self, value):
        self._run_time = value

    def _get_run_time(self):
        return self._run_time

    run_time = property(_get_run_time, _set_run_time)

    def _set_test_card_model(self, value):
        self._test_card_model = value

    def _get_test_card_model(self):
        return self._test_card_model

    test_card_model = property(_get_test_card_model, _set_test_card_model)

    # Utility functions
    def save(self, file_name):
        """
        Save the TestInstance properties to a file

        Keyword arguments:
        self -- instance of TestInstance
        file_name -- the name of the file to write the arguments to
        """
        with open(file_name, 'w') as test_cfg:
            config = configparser.ConfigParser()
            config['TEST_INSTANCE'] = {'client_ip': self.client_ip,
                                       'server_ip': self.server_ip,
                                       'run_time': self.run_time,
                                       'test_card_model': self.test_card_model}
            config.write(test_cfg)

    def load(self, file_name):
        """
        Load the TestInstance properties from a file

        Keyword arguments:
        self -- instance of TestInstance
        file_name -- the name of the file to load the arguments from
        """
        config = configparser.ConfigParser()
        try:
            with open(file_name, 'r') as test_cfg:
                config.read(test_cfg.name)
                self.client_ip = config['TEST_INSTANCE']['client_ip']
                self.server_ip = config['TEST_INSTANCE']['server_ip']
                self.run_time = config['TEST_INSTANCE']['run_time']
                self.test_card_model = config['TEST_INSTANCE'][
                    'test_card_model']
                return True
        except configparser.MissingSectionHeaderError:
            menu_backend.notification_caller(
                'ERROR: test_net.cfg does not contain a valid configuration'
                ' section.')
            return False
        except configparser.Error:
            menu_backend.notification_caller(
                'ERROR: test_net.cfg is an invalid configuration file.')
            return False
