"""
This is the core processor testing module.  It runs a predefined set of tests,
saves the results to a master JSON results file, and outputs .png images
representing the test results.
"""

import json
import os
import subprocess
import sys
import time

import universal_functions

__author__ = 'sean'

# TODO create reference data


def pbzip2_test(num_threads):
    """
    Run the pbzip2 compression test.

    :param num_threads: an integer representing the number of threads to be used
    for the test
    :return: results_dict: dictionary containing test run information
    """

    # Create test file for pbzip2 test
    try:
        print('Writing test file...')
        subprocess.check_call(
            'dd if=/dev/urandom of=cpu_pbzip2_test_file bs=1M count=1024',
            shell=True, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        print('ERROR:\tfailed to create test file for pbzip2!')
        sys.exit(-1)

    print('Running pbzip2 test...')
    result_list = []
    for i in range(1, 11):
        try:
            start = time.time()
            subprocess.check_call(['pbzip2', '-c', '-p' + str(num_threads),
                                   '-r', '-9', 'cpu_pbzip2_test_file'],
                                  stdout=subprocess.DEVNULL)
            finish = time.time()
            elapsed = finish - start
            result_list.append(elapsed)
            print('Run', str(i), 'time elapsed:\t',
                  str('{:.2f}'.format(elapsed)), 'seconds')
        except subprocess.CalledProcessError as cpe:
            print('ERROR:\tpbzip2 encountered an error!')
            print('\t\tExit status:', cpe.returncode)
            sys.exit(cpe.returncode)
    results_dict = universal_functions.get_results_dict(
        result_list,
        'Test Run',
        'Time (seconds)',
        'pbzip2 Compression Test',
        universal_functions.get_cpu_model())
    results_dict['num_threads'] = num_threads

    print('Mean:\t\t\t', str('{:.2f}'.format(results_dict['mean'])),
          'seconds')
    print('Median:\t\t\t', str('{:.2f}'.format(results_dict['median'])),
          'seconds')
    print('Standard Deviation:\t', str('{:.2f}'.format(results_dict['stddev'])),
          'seconds')

    print('Removing output file...')
    subprocess.check_call(['rm', '-f', 'cpu_pbzip2_test_file'])

    return results_dict


def tachyon_test(num_threads, tachyon_file):
    """
    Run the tachyon ray trace test.

    :param num_threads: an integer representing the number of threads to be used
    for the test
    :param tachyon_file: the tachyon file to be used for the test run
    :return: results_dict: dictionary containing test run information
    """
    # Test opening the test file
    # Tachyon will not throw an error if the file path is invalid
    # Rather, it will simply complete immediately
    try:
        fp = open(tachyon_file)
        fp.close()
        print('Test file is accessible.  Continuing...')
    except PermissionError:
        print('ERROR:\tfailed to open {} due to a permission error!'
              .format(tachyon_file))
        sys.exit(-1)
    except FileNotFoundError:
        print('ERROR:\ttest file, {}, not found!'
              .format(tachyon_file))
        sys.exit(-1)

    print('Running tachyon test...')
    result_list = []
    for i in range(1, 11):
        try:
            start = time.time()
            # Why did I have -numthreads commented out?
            subprocess.check_call(['tachyon', '-numthreads', str(num_threads),
                                  tachyon_file, '-fullshade', '-shade_phong',
                                   '-trans_vmd', '-aasamples', '32', '-res',
                                   '1080', '1080', '-o',
                                   'cpu_tachyon_test_file'],
                                  stdout=subprocess.DEVNULL)
            finish = time.time()
            elapsed = finish - start
            result_list.append(elapsed)
            print('Run', str(i), 'time elapsed:\t',
                  str('{:.2f}'.format(elapsed)), 'seconds')
        except subprocess.CalledProcessError as cpe:
            print('ERROR:\ttachyon encountered an error!')
            print('\t\tExit status:', cpe.returncode)
            sys.exit(cpe.returncode)

    results_dict = universal_functions.get_results_dict(
        result_list,
        'Test Run',
        'Time (seconds)',
        'Tachyon Ray Trace Test',
        universal_functions.get_cpu_model())
    results_dict['num_threads'] = num_threads

    print('Removing output file...')
    subprocess.check_call(['rm', '-f', 'cpu_tachyon_test_file'])

    print('Mean:\t\t\t', str('{:.2f}'.format(results_dict['mean'])),
          'seconds')
    print('Median:\t\t\t', str('{:.2f}'.format(results_dict['median'])),
          'seconds')
    print('Standard Deviation:\t', str('{:.2f}'.format(results_dict['stddev'])),
          'seconds')
    return results_dict


def main():
    # Determine number of threads to be used for tests
    # https://docs.python.org/3/library/os.html#os.cpu_count
    try:
        cpu_threads = os.cpu_count()
        if not cpu_threads:
            print('ERROR:\tunable to determine the number of CPUs.  '
                  'os.cpu_count() requires Python 3.4+!')
            print('Defaulting to two threads...')
            cpu_threads = 2
    except NotImplementedError:
        print(
            'ERROR:\tos.cpu_count() not implemented.  Python 3.4+ is required!')
        print('Defaulting to two threads...')
        cpu_threads = 2

    if os.path.exists('./output'):
        output_path = './output/'
    else:
        output_path = ''

    tachyon_dict = tachyon_test(cpu_threads, './input/teapot.dat')
    universal_functions.results_to_bar_graph(
        tachyon_dict,
        '{}processor-tachyon'.format(output_path))

    pbzip2_dict = pbzip2_test(cpu_threads)
    universal_functions.results_to_bar_graph(
        pbzip2_dict,
        '{}processor-pbzip2'.format(output_path))

    try:
        with open('raw_results.json', 'r+') \
                as results_file:
            try:
                print('Updating master JSON results file...')
                json_dict = json.load(results_file)
                json_dict['processor_tachyon'] = tachyon_dict
                json_dict['processor_pbzip2'] = pbzip2_dict
                results_file.seek(0)
                json.dump(json_dict, results_file, sort_keys=True, indent=4)
            except ValueError:
                print('ERROR:\tvalue error!  File may be empty or contain '
                      'invalid JSON.  Will overwrite with current results.')
                json_dict = {'processor_tachyon': tachyon_dict,
                             'processor_pbzip2': pbzip2_dict}
                results_file.seek(0)
                json.dump(json_dict, results_file, sort_keys=True, indent=4)
            results_file.truncate()
    except FileNotFoundError:
        print('ERROR:\tfile raw_results.json does not exist.  Creating...')
        with open('raw_results.json', 'a+') as new_results_file:
            json_dict = {'processor_tachyon': tachyon_dict,
                         'processor_pbzip2': pbzip2_dict}
            json.dump(json_dict, new_results_file, sort_keys=True, indent=4)

if __name__ == '__main__':
    main()
