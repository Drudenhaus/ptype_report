## Required packages
* python3 (3.4 used for development and API reference)
    * matplotlib
    * numpy
* tachyon
* pbzip2
* ramspeedsmp
* iperf3
* fio (flexible i/o tester)

## Notes

* Start the iperf3 server with JSON argument.  e.g.

	iperf3 -s --bind 192.168.0.100 --json