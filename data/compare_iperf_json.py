"""
This is the core network testing module.  It obtains the iperf parameters
through the use of the menu test_net_menu, runs the iperf test itself, parses
the JSON output, graphs the data, and outputs .png images representing the test
results.
"""
# These represent python3 features that have been backported to python 2.6+
# They must be at the top of the file since they are compiler directives rather
#   than standard imports.  This is also why they cannot be inside a try/except
#   or other block of code (e.g. detect version of python and only import when
#   >= 2.6 and <= 3.0).
# Explanations:
# unicode_literals: legacy python is stupid and doesn't use unicode by default.
#   The jitter graph uses the Greek mu character to represent micro.
# print_function:
#   python2 print = statement; arguments in a python2 print statement are
#     interpreted as a tuple.
#   python3 print = function; arguments in a python2 print statement are
#       function parameters.
#   This way all print outputs will look the same regardless of which version is
#     used.
# Reference: https://docs.python.org/3/reference/simple_stmts.html#future

from __future__ import unicode_literals
from __future__ import print_function

import numpy
import json
import collections
import math
import matplotlib as mpl
# Use the agg backend so we don't need to rely on an X server/session
# This needs to be done before the pyplot import else the backend won't change
# and a warning is thrown
mpl.use('Agg')
import matplotlib.pyplot as plt

__author__ = 'sean'

# Get the test parameters
iperf_parameters = collections.OrderedDict([
    ('Client IP Address', '192.168.0.2'),
    ('Server IP Address', '192.168.0.100'),
    ('Run Time', '910'),
    ('Test Card Model', 'Intel X540-AT2'),
    ('Done', 'Run Test'),
    ('Save', 'Save to test_net.cfg'),
    ('Load', 'Load from test_net.cfg'),
    ('Quit', 'Exit to shell')
])


def log_to_line_graph(model1, results1, model2, results2, xlabel, ylabel,
                      title, filename):
    """
    Take the results and produce a graphical representation

    :param results: the results to be graphed
    :param xlabel: the label for the x axis of the graph
    :param ylabel: the label for the y axis of the graph
    :param title: the title for the graph
    :param model: the model of the card that was tested
    :param filename: the file name for the .png image
    :return:
    """
    plt.clf()
    ax = plt.subplot(111)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    if len(results1) > 0 and len(results2) > 0:
        print(model1, 'mean:', numpy.mean(results1), ylabel)
        print(model2, 'mean:', numpy.mean(results2), ylabel)
        print(model1, 'std dev:', numpy.std(results1), ylabel)
        print(model2, 'std dev:', numpy.std(results2), ylabel)

        plt.plot(results1, label=model1)
        plt.plot(results2, label=model2)
        ymin, ymax = plt.ylim()
        plt.ylim(ymin, ymax * 1.1)

        # Shrink the figure size so that the legend fits below it
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width,
                         box.height * 0.85])

        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), fancybox=True,
                  ncol=len(results1), fontsize=10)

        # Set the title
        plt.title(title)
        plt.savefig(filename + '.png', format='png')
        print('Saved:\t{}.png'.format(filename))
    else:
        print('ERROR\tthe list is empty!')


# Convert bits per second to megabits per second
# Round down and return an int
def bits_to_mbits_per_second(bits):
    """
    Take a number of bits and return the converted quantity of megabits

    :param bits: an integer representing a quantity of bits
    :return: an integer representing bits converted to megabits
    """
    return math.floor(bits / math.pow(1000, 2))


def ms_to_us(ms):
    """
    Take a number of milliseconds and return the converted quantity of
    microseconds.

    :param ms: an integer representing a quantity of milliseconds.
    :return: an integer representing bits converted to microseconds.
    """
    return ms * math.pow(1000, 1)


# Get bits per second results and convert each result to mbits per second
def get_mbits_per_second(json_blob, the_dict):
    """
    Pull the megabits per second results out of the JSON results and,
    ultimately, return a list of those results

    :param json_blob: the raw JSON output from iperf.
    :param the_dict: resulting dict from the interval key of the JSON results.
    :return: the megabits per second results in a list
    """

    the_list = []
    # Skip the first 10 seconds (the test will stabilize by this time)
    for position in range(10, len(the_dict)):
        bits = json_blob['intervals'][position]['sum']['bits_per_second']
        mbits = bits_to_mbits_per_second(bits)
        the_list.append(mbits)
    return the_list


# Get get latency results in ms results and convert each result to us
def get_jitter_us(json_blob, the_dict):
    """
    Pull the jitter results out of the JSON results and, ultimately, return a
    list of those results

    :param json_blob: the raw JSON output from iperf.
    :param the_dict: resulting dict from the interval key of the JSON results.
    :return: the jitter results in a list
    """

    the_list = []
    # Skip the first 10 seconds (the test will stabilize by this time)
    for position in range(10, len(the_dict)):
        ms = json_blob['server_output_json']['intervals'][position]['streams'][
            0]['jitter_ms']
        us = ms_to_us(ms)
        the_list.append(us)
    return the_list


# Get and graph the bandwidth results
with open('net_bw.json1', 'r') as net_bw_json_file1, \
        open('net_bw.json2', 'r') as net_bw_json_file2:
    net_bw_json_blob1 = json.load(net_bw_json_file1,
                                  object_pairs_hook=collections.OrderedDict)
    net_bw_list1 = get_mbits_per_second(net_bw_json_blob1,
                                        net_bw_json_blob1['intervals'])

    net_bw_json_blob2 = json.load(net_bw_json_file2,
                                  object_pairs_hook=collections.OrderedDict)
    net_bw_list2 = get_mbits_per_second(net_bw_json_blob2,
                                        net_bw_json_blob2['intervals'])

    log_to_line_graph('Intel X540-AT2',
                      net_bw_list1,
                      'QLogic QLE3442',
                      net_bw_list2,
                      'Time (seconds)',
                      'Megabits',
                      'iperf (bandwidth)',
                      'net-bandwidth')

# Get and graph the jitter results
with open('net_jitter.json1', 'r') as net_jitter_json_file1, \
        open('net_jitter.json2', 'r') as net_jitter_json_file2:
    net_jitter_json_blob1 = json.load(net_jitter_json_file1,
                                      object_pairs_hook=collections.OrderedDict)
    net_jitter_list1 = get_jitter_us(net_jitter_json_blob1,
                                     net_jitter_json_blob1['server_output_json']
                                     ['intervals'])
    net_jitter_json_blob2 = json.load(net_jitter_json_file2,
                                      object_pairs_hook=collections.OrderedDict)
    net_jitter_list2 = get_jitter_us(net_jitter_json_blob2,
                                     net_jitter_json_blob2['server_output_json']
                                     ['intervals'])
    log_to_line_graph('Intel X540-AT2',
                      net_jitter_list1,
                      'QLogic QLE3442',
                      net_jitter_list2,
                      'Time (seconds)',
                      'Jitter (\u00B5s)',
                      'iperf (jitter)',
                      'net-jitter')